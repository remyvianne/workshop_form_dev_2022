import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MenuItem, MessageService, PrimeNGConfig} from 'primeng/api';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.sass'],
  providers: [MessageService]
})
export class ModulesComponent implements OnInit {
  disabled: boolean = true;
  value1: string = '';
  items: MenuItem[] = [];

    constructor(private router: Router, private messageService: MessageService,private primengConfig: PrimeNGConfig) {}

    ngOnInit() {
        this.primengConfig.ripple = true;

        this.items = [{
                label: 'Mon compte',
                icon: 'pi pi-user-edit',
                command: () => {
                    this.dashboard();
                }
            },
            {
                label: 'Déconnexion',
                icon: 'pi pi-sign-out',
                command: () => {
                    this.disconnect();
                }
            }
        ];
    }

    dashboard() {
        this.messageService.add({severity:'success', summary:'Dashboard', detail:'User dashboard'});
        this.router.navigate(['/dashboard']);
    }

    disconnect() {
        this.messageService.add({severity:'info', summary:'Disconnect', detail:'User disconnected'});
        this.router.navigate(['/authentification']);
    }
}

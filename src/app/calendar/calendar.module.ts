import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent } from './calendar/calendar.component';
import { CalendarHeaderComponent } from './calendar-header/calendar-header.component';
import { CalendarMonthComponent } from './calendar-month/calendar-month.component';
import { CalendarWeekComponent } from './calendar-week/calendar-week.component';
import { CalendarListComponent } from './calendar-list/calendar-list.component';
import { CalendarListItemComponent } from './calendar-list-item/calendar-list-item.component';

import { PanelModule } from 'primeng/panel';
import { TabMenuModule } from 'primeng/tabmenu';




@NgModule({
  declarations: [
    CalendarComponent,
    CalendarHeaderComponent,
    CalendarMonthComponent,
    CalendarWeekComponent,
    CalendarListComponent,
    CalendarListItemComponent,
    
  ],
  imports: [
    CommonModule,
    PanelModule,
    TabMenuModule,
  ]
})
export class CalendarModule { }

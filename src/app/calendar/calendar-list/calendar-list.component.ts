import { Component, OnInit } from '@angular/core';
import { CalendarService } from '../calendar.service';
import { Calendar } from '../calendar.model';


@Component({
  selector: 'app-calendar-list',
  templateUrl: './calendar-list.component.html',
  styleUrls: ['./calendar-list.component.sass']
})
export class CalendarListComponent implements OnInit {

  calendars: Calendar[] = [];

  constructor(private calendarService: CalendarService) { }

  ngOnInit(): void {
    const calendatObservable = this.calendarService.getCalendars();
    calendatObservable.subscribe((calendarsData: Calendar[]) => {
      this.calendars = calendarsData;
    });
  }

}

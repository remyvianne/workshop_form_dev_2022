import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-calendar-list-item',
  templateUrl: './calendar-list-item.component.html',
  styleUrls: ['./calendar-list-item.component.sass']
})
export class CalendarListItemComponent implements OnInit {

  @Input() calendar: any;

  constructor() { }

  ngOnInit(): void {
  }

}

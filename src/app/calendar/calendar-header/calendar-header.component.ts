import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';


@Component({
  selector: 'app-calendar-header',
  templateUrl: './calendar-header.component.html',
  styleUrls: ['./calendar-header.component.sass']
})
export class CalendarHeaderComponent implements OnInit {

  items: MenuItem[] = [];


  constructor() { }

  ngOnInit(): void {
    this.items = [
      { label: 'Calendrier', icon: 'pi pi-fw pi-calendar' },
      { label: 'Agenda', icon: 'pi pi-fw pi-book' },
    ];
  }

}

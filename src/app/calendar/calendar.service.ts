import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Calendar } from './calendar.model';

@Injectable({
  providedIn: 'root'
})

export class CalendarService {

  calendars: Calendar[] = [
    {
      id: 1,
      name: 'Base de données',
      professor: 'M. Pirouette',
      hourStart: '09:00',
      hourEnd: '12:30',
      date: 'Lundi 27 Juin 2022'
    },
    {
      id: 2,
      name: 'Base de données',
      professor: 'M. Pirouette',
      hourStart: '14:00',
      hourEnd: '17:30',
      date: 'Lundi 27 Juin 2022'
    },
    {
      id: 3,
      name: 'Base de données',
      professor: 'M. Pirouette',
      hourStart: '09:00',
      hourEnd: '12:30',
      date: 'Mardi 28 Juin 2022'
    },
    {
      id: 4,
      name: 'Base de données',
      professor: 'M. Pirouette',
      hourStart: '14:00',
      hourEnd: '17:30',
      date: 'Mardi 28 Juin 2022'
    },
    {
      id: 5,
      name: 'Base de données',
      professor: 'M. Pirouette',
      hourStart: '09:00',
      hourEnd: '12:30',
      date: 'Mercredi 29 Juin 2022'
    },
    {
      id: 6,
      name: 'Base de données',
      professor: 'M. Pirouette',
      hourStart: '14:00',
      hourEnd: '17:30',
      date: 'Mercredi 29 Juin 2022'
    },
    {
      id: 7,
      name: 'Développpemnt Web',
      professor: 'M. Pirouette',
      hourStart: '09:00',
      hourEnd: '12:30',
      date: 'Jeudi 30 Juin 2022'
    },
    {
      id: 8,
      name: 'Développpemnt Web',
      professor: 'M. Pirouette',
      hourStart: '14:00',
      hourEnd: '17:30',
      date: 'Jeudi 30 Juin 2022'
    },
    {
      id: 9,
      name: 'FullStack',
      professor: 'M. Pirouette',
      hourStart: '09:00',
      hourEnd: '12:30',
      date: 'Vendredi 01 Juillet 2022'
    },
    {
      id: 10,
      name: 'FullStack',
      professor: 'M. Pirouette',
      hourStart: '14:00',
      hourEnd: '17:30',
      date: 'Vendredi 01 Juillet 2022'
    }
  ];

  constructor() { }

  public getCalendars(): any {
    const calendarObeservable = new Observable(observer => {
      setTimeout(() => {
        observer.next(this.calendars);
      }
        , 1000);
    });
    return calendarObeservable;
  }
}

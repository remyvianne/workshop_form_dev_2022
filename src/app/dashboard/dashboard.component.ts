import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  displayModal:boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  showResetPasswordModal() {
    this.displayModal = true;
  }
}

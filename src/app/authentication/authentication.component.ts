import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import SignaturePad from 'signature_pad';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.sass'],
})
export class AuthenticationComponent implements OnInit {

  signaturePad?: SignaturePad;
  @ViewChild('canvas') canvasEl?: ElementRef;
  signatureImg: string = "";
  courseName: string = "Cours de test";
  displayModal: boolean = false;

  display: boolean = false;

  constructor() {

  }

  ngOnInit() {

  }

  showDialog() {
    this.display = true;
  }
  dismissDialog() {
    this.display = false;
  }


  // For Course Modal
  ngAfterViewInit() {
    this.signaturePad = new SignaturePad(this.canvasEl?.nativeElement);
  }
  showCourseModal() {
    this.displayModal = true;
  }
  startDrawing(event: Event) {
    console.log(event);
  }
  moved(event: Event) {
    // works in device not in browser
  }
  clearPad() {
    this.signaturePad?.clear();
  }
  savePad() {
    const base64Data = this.signaturePad?.toDataURL();
    this.signatureImg = base64Data ?? "";
    this.displayModal = false;
  }

}
